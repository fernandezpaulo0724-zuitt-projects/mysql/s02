-- List down the databases inside the DBMS.
SHOW DATABASES;

-- Create a database
CREATE DATABASE music_db;

-- Delete a Database
DROP DATABASE music_db;

-- Select a database
USE music_db;

-- Create tables
-- Table columns have the following format: [column_name] [data_type] [other_options]
CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    pssword VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number INT NOT NULL,
    email VARCHAR(50) NOT NULL,
    address VARCHAR(50),
    PRIMARY KEY (id)
);

CREATE TABLE playlists_songs (
	id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_songs_playlist_id
    	FOREIGN KEY (playlist_id) REFERENCES playlists(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_playlists_songs_song_id
    	FOREIGN KEY (song_id) REFERENCES songs(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);

